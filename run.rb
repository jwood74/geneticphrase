#!/usr/bin/ruby

require_relative 'commands'

$totalPopulation = 1000
$mutation = 0.02

target = ''
targetlen = 0
answerfound = false
DNAs = DNA.new

count = 0
while count < 10
	puts 'Type in a phrase for ruby to search for- '
	target = gets.chomp
	puts 'Your phrase has ' + target.length.to_s + ' characters.'
	posi = possibilities(target.length)
	puts 'there are ' + prettynum(posi).to_s + ' possible phrases of this length.'
	puts 'this would take ' + timetake(posi) + ' to brute force'
	puts 'Is this your final answer? y/n'
	finans = gets.chomp
#	finans = 'y'
	if finans == 'y' || finans == 'Y'
		break
	end
	count += 1
end

$population = Array.new($totalPopulation)
$fitness = Array.new($totalPopulation)
$pool = Array.new($totalPopulation)

$totalPopulation.times do | count |
	$population[count] = DNAs.NewGenes(target.length)
end

1000.times do |count|
	DNAs.calculate_fitness(target)
	DNAs.evaluate(target, count)
	if $fitness.max == 1
		break
	end
	DNAs.reproduction(target)
	if $fitness.max == 1
		break
	end
end