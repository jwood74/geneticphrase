# README #

### What is this repository for? ###

* This is my first attempt at a Genetic Algorithm to Evolve a Phrase
* More info in this YouTube [playlist](https://www.youtube.com/playlist?list=PLRqwX-V7Uu6bJM3VgzjNV5YxVxUwzALHV)
* Version 1

### What is happening? ###

* You type in a phrase
* It tells you how long it would take to randomly guess that phrase using brute force
* It then makes a random population of size N (default 1000)
* Every item in the population is given a score based on percent of letters in correct place
* Using this score, a mating pool is created filled mostly with high scoring results, and less with low scoring results
* A new population is created from this mating pool, randomly merging two parents with a tiny amount of mutation
* With enough generations and population size, it *should* eventually find your original phrase (certainly quicker than the bruce force time)

### Set up ###

* Requires ruby
* once the repository is in place, run with 'ruby run.rb'

### ToDo ###

* Add Accept/Reject method of selection

### Who do I talk to? ###

* jwood74 - Jaxen Wood - jw@jaxenwood.com