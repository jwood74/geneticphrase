#!/usr/bin/ruby

def possibilities(length)
	len = length
	a = 1.to_f / 97
	b = a.to_f ** len
	c = 1.to_f / b
	d = "%i" % c
	return d
end

def avg(array)
	sum = 0
	array.each do |cnt|
		sum += cnt.to_f
	end
	av = sum.to_f / array.length
	return av
end

def prettynum(number)
	parts = number.split('.')
	parts[0].gsub!(/(\d)(?=(\d\d\d)+(?!\d))/, "\\1,")
	parts.join('.')
	return parts.first
end

def timetake(posi)
	persec = posi.to_f / 1000000
	permin = persec / 60
	perhou = permin / 60
	perday = perhou / 24
	peryea = perday / 365.25
	if perhou <= 1
		num = "%i" % permin
		return prettynum(num) + ' minutes'
	elsif perday <= 1
		num = "%i" % perhou
		return prettynum(num) + ' hours'
	elsif peryea <= 1
		num = "%i" % perday
		return prettynum(num) + ' days'
	else
		num = "%i" % peryea
		return prettynum(num) + ' years'
	end
end

class DNA
	def NewGenes(len)
		genes = []
		len.times do | i |
			genes[i] = rand(32..122).chr
		end
		return genes
	end

	def GetPhrase(phrase)
		return phrase.join("")
	end

	def calculate_fitness(targ)
		$fitness = []
		$totalPopulation.times do | count |
			num = 0
			targ.length.times do | char |
				if $population[count][char] == targ[char]
					num += 1
				end
			end
			$fitness[count] = num.to_f / targ.length
		end
	end

	def evaluate(targ, cnt)
		if $fitness.max > 0
				puts cnt.to_s + ' Generation. BEST- "' + GetPhrase($population[$fitness.each_with_index.max[1]]) + '"- ' + ($fitness.max * 100).to_i.to_s + '% match. Average- ' + (avg($fitness) * 100).to_i.to_s + '%'
		end
		return $fitness.max
	end

	def selectionBucket(targ)
		pool = Array.new
		$totalPopulation.times do | count |
			tmp = $fitness[count] * 100
			tmp.to_i.times do | cnt |
				pool << $population[count]
			end
		end
		if pool.length == 0
			exit
		end
		return pool
	end

	def reproduction(targ)
		pool = DNAs.selectionBucket(targ)
		newpop = Array.new($totalPopulation)
		$totalPopulation.times do | count |
			a = rand(0..pool.length-1)
			b = rand(0..pool.length-1)
			parentA = pool[a]
			parentB = pool[b]

			newpop[count] = crossover(parentA,parentB,targ.length)
			newpop[count] = mutate(newpop[count])
		end
		$population = newpop
	end

	def crossover(parentA,parentB,num)
		child = Array.new(num)
		midpoint = rand(0..num)
		num.times do | i |
			if i >= midpoint
				child[i] = parentA[i]
			else
				child[i] = parentB[i]
			end
		end
		return child
	end

	def mutate(element)
		element.length.times do | i |
			if rand(0.0..1.0) < $mutation
				element[i] = rand(32..122).chr
			end
		end
		return element
	end
end